(ns blogger-fetcher.storage
  (:require [clojure.java.io :refer [file make-parents]]
            [datalevin.core :as d]
            [blogger-fetcher.storage :as storage]))

(defprotocol Storage
  (upsert-post [this id post]
    "Store post"))

(defn in-memory-storage
  "Create in-memory implementaton of Storage protocol."
  []
  (let [!storage (atom {})]
    (reify Storage
      (upsert-post
        [_ id post]
        (swap! !storage assoc id post)
        id))))

(defn file-storage
  "Create file based implementation of Storage protocol."
  [data-dir]
  (reify Storage
    (upsert-post
      [_ id post]
      (let [storage-dir (file data-dir)
            storage-file (file storage-dir (str id ".edn"))]
        (make-parents storage-file)
        (spit storage-file post)
        id))))

(defn datalevin-storage
  "Create Datalevin based implementation of Storage protocol"
  [conn]
  (reify Storage
    (upsert-post
      [_ id post]
      (d/transact! conn [post])
      id)))

(comment
  (def conn (atom {}))

  (d/transact! conn [{:db/add [:id "a"] :blog/id "a" :id "a" :url "/b" :blog/title "A/C title"}])

  (or false "1")

  (d/transact! conn [[:db/add 1 :name "Ivan1"]])

  (d/transact! conn [[:db/add "ivan" :name "Ivan"]])
  (:tempids *1)

  (d/transact! conn [{:db/id 1367 :name "Iv"}])

  (d/transact! conn [{:db/id -1 :id 1 :url "a"}])
  (d/transact! conn [[:db/retract 1371 :id 1]])

  (d/entity @conn 1367)
  (into {} *1)
  (println *2)

  (d/datoms @conn :eav 11)

  (d/q '[:find ?e
         :where
         [?e :id "7356010244958013577"]]
       @conn)

  (d/q '[:find ?e :in $ ?id :where [?e :id ?id]] @conn "a")

  (:labels (d/entity @conn 11))
  (:url (d/entity @conn [:id "b"]))

  (def id "7356010244958013577")
  (d/entity @conn [:id id])
  (keys *1)
  (:db/id *2)

  (type *1)
  (into {} *2)

  (d/q '[:find (pull ?e [*])
         :where
         [?e :id "7356010244958013577"]]
       @conn)

  (def entity-id 1356)
  (d/q '[:find ?e
         :where
         [?e :db/id "1356"]]
       @conn)

  (d/q '[:find ?id
         :where [_ :id ?id]]
       @conn)

  (d/q '[:find ?url
         :where
         ;;[?e :title "Gradle Goodness: Enabling Preview Features For Java"]
         [?e :labels "Spock"]
         [?e :url ?url]]
       @conn)

  (-> (d/q '[:find ?url
             :where
             ;;[?e :title "Gradle Goodness: Enabling Preview Features For Java"]
             [?e :labels "Clojure"]
             [?e :url ?url]]
           @conn)
      vec
      flatten)

  (d/q '[:find (count ?e)
         :where
         [?e :labels "Gradle:Goodness"]]
       @conn)

  (take-last 33 (sort (d/q '[:find ?url ?title
                             :where
                             [?e :url ?url]
                             [?e :title ?title]
                             [?e :labels "Gradle:Goodness"]]
                           @conn)))

  (map second (take-last 33 (sort (d/q '[:find ?url ?title
                                         :where
                                         [?e :url ?url]
                                         [?e :title ?title]
                                         [?e :labels "Gradle:Goodness"]]
                                       @conn))))

  (sort (d/q '[:find ?url ?title
               :where
               [?e :url ?url]
               [?e :title ?title]
               [?e :labels "Gradle:Goodness"]]
             @conn)))
