(ns blogger-fetcher.system
  (:require [com.stuartsierra.component :as component]
            [datalevin.core :as d]
            [clojure.tools.logging :as log]))

(defrecord Database [storage-dir connection]
  component/Lifecycle
  
  (start
    [component]
    (log/infof "Starting database %s" storage-dir)
    (if connection
      component
      (let [schema {:id     {:db/valueType :db.type/string
                             :db/unique    :db.unique/identity}
                    :url    {:db/valueType :db.type/string
                             :db/unique    :db.unique/identity}
                    :labels {:db/valueType   :db.type/string
                             :db/cardinality :db.cardinality/many}}]
        (assoc component :connection (d/get-conn storage-dir schema)))))
  
  (stop
    [component]
    (log/info "Stopping database")
    (if (d/closed? connection)
      (assoc component :connection nil)
      (assoc component :connection (d/close connection)))))

(defn- new-database
  "Create new database component."
  [storage-dir]
  (map->Database {:storage-dir storage-dir}))

(defrecord BloggerCrawler [api-key blogger-id host]
  component/Lifecycle
  
  (start
    [component]
    component)
  
  (stop
    [component]
    component))
  
(defn- new-crawler
  "Create new crawler component."
  [api-key blogger-id host]
  (map->BloggerCrawler {:api-key    api-key
                        :blogger-id blogger-id
                        :host       host}))

(defn development-system
  "Configure development system with test database and crawler components."
  [config]
  (let [{crawler-config :blogger} config]
    (component/system-map
     :database (new-database "./build/blogger-db")
     :crawler (new-crawler (:api-key crawler-config) (:blogger-id crawler-config) "https://www.googleapis.com"))))

(defn production-system
  "Configure production system with database and crawler components."
  [config]
  (let [{database-config :database
         crawler-config :blogger} config]
    (component/system-map
      :database (new-database (:storage-dir database-config))
      :crawler (new-crawler (:api-key crawler-config) (:blogger-id crawler-config) "https://www.googleapis.com"))))

(defn start
  "Performs side effects to initialize the system, acquire resources,
  and start it running. Returns an updated instance of the system."
  [system]
  (component/start system))

(defn stop
  "Performs side effects to shut down the system and release its
  resources. Returns an updated instance of the system."
  [system]
  (component/stop system))