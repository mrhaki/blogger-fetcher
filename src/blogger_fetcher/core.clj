(ns blogger-fetcher.core
  (:require [blogger-fetcher.blogger :as blogger]
            [blogger-fetcher.storage :as st]
            [blogger-fetcher.system :as system]
            [com.stuartsierra.component :as component]
            [cprop.core :refer [load-config]]
            [clojure.tools.logging :as log])
  (:gen-class))

(defn- fetch-blogger-posts
  "Get all posts."
  [crawler]
  (blogger/get-posts crawler))

(defn- upsert-posts
  "Insert or update posts in the database."
  [storage posts]
  (doseq [post posts]
    (st/upsert-post storage (:id post) post)))

(defn- fetch-and-save
  "Get all posts and save them."
  [crawler storage]
  (let [posts (fetch-blogger-posts crawler)]
    (log/infof "Saving %d posts." (count posts))
    (upsert-posts storage posts)))

(defn get-config
  "Load configuration."
  []
  (load-config :file "~/.blogger.edn"))

(defn -main
  "Fetch blogger posts and save in database"
  [& _]
  (let [config (get-config)
        system (component/start (system/production-system config))
        database (:database system)
        crawler (:crawler system)]
    (fetch-and-save crawler (st/datalevin-storage (:connection database)))
    (component/stop system)))

(comment
  (-main)
  (:connection *1)

  (def config (get-config))
  (config)
  (def conn (:connection (:database sys)))



  (def sys (system/production-system (get-config)))
  (alter-var-root #'sys component/start)
  (println (get-in sys [:crawler]))
  (alter-var-root #'sys component/stop)
  (:database sys)
  (:crawler sys)
  (component/start sys)
  (component/stop sys)

  (fetch-blogger-posts (:crawler sys))

  (fetch-blogger-posts {})

  (def in-memory-storage (st/in-memory-storage))
  (upsert-posts in-memory-storage [{:id "a" :content "A"}])

  (def file-storage (st/file-storage "posts"))
  (upsert-posts file-storage [{:id "a" :content "B"}])

  (fetch-and-save config file-storage)
  

  (def blogger-post {:id        "7356010244958013577",
                     :published "2021-03-05T22:44:00+01:00",
                     :updated   "2021-03-05T22:48:55+01:00",
                     :url       "http://blog.mrhaki.com/2021/03/gradle-goodness-enabling-preview.html",
                     :title     "Gradle Goodness: Enabling Preview Features For Java",
                     :content   "<p>Java introduced preview features in the language since Java 12.
                    This features can be tried out by developers, but are still subject to change and can even be removed in a next release.
                    By default the preview features are not enabled when we want to compile and run our Java code.
                    We must explicitly specify that we want to use the preview feature to the Java compiler and Java runtime using the command-line argument <code>--enable-preview</code>.
                    In Gradle we can customize our build file to enable preview features.
                    We must customize tasks of type <code>JavaCompile</code> and pass <code>--enable-preview</code> to the compiler arguments.
                    Also tasks of type <code>Test</code> and <code>JavaExec</code> must be customized where we need to add the JVM argument <code>--enable-preview</code>.</p>
                    <p>Written with Gradle 6.8.3</p><p>Java 11.</p>
                    ",
                     :labels    ["Gradle" "Java"
                                 "Gradle 6.8.3"
                                 "Gradle:Goodness"
                                 "GradleGoodness:Java"
                                 "GradleGoodness:Kotlin"],
                     :etag      "\"dGltZXN0YW1wOiAxNjE0OTgwOTM1MzI4Cm9mZnNldDogMzYwMDAwMAo\""})

  (println blogger-post)
  (println [1 2 3])
  (println {:a 1 :b 2})
  ,)
  
