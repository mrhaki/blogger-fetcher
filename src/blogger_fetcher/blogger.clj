(ns blogger-fetcher.blogger
  (:require [clj-http.client :as http]
            [cheshire.core :as json]
            [clojure.string :as str]
            [clojure.tools.logging :as log]))

(defn- is-empty-page-token?
  "Check if given token is empty."
  [token]
  (str/blank? (str token)))

(def is-not-empty-page-token? (complement is-empty-page-token?))

(defn- get-next-page-token
  "Return token or nil if token equals :start keyword."
  [token]
  (when-not (= :start token) token))

(defn- create-query-params
  "Create the HTTP GET query parameters to get blog post data.
   We set the maximum results per query to 100 results.
   For each post we are only interested in the following attributes:
   id, published, updated, url, title, content, labels and etag."
  [api-key next-page-token]
  (into {} (filter (fn [[_ v]] (some? v))
                   {:key api-key
                    :maxResults 100
                    :fields "nextPageToken,items(id,published,updated,url,title,content,labels,etag)"
                    :pageToken (get-next-page-token next-page-token)})))

(defn- list-posts
  [blog-id api-key host next-page-token]
  (let [api (str host "/blogger/v3/blogs/" blog-id "/posts")
        query-params (create-query-params api-key next-page-token)
        response (http/get api {:query-params query-params})
        result (json/parse-string (:body response) true)]
    result))

(defn get-posts
  "Get all posts from Blogger and return as collection with all data."
  [crawler]
  (let [{:keys [blogger-id api-key host]} crawler]
    (loop [next-page-token :start
           posts []] 
      (let [result (list-posts blogger-id api-key host next-page-token)
            items (:items result)
            next-page-token* (:nextPageToken result)
            posts* (apply conj posts items)]
        (log/infof "Fetching posts with next-page-token '%s'" next-page-token*)
        (if (not (str/blank? next-page-token*))
          (recur next-page-token* posts*)
          posts*)))))

(comment

  (defn api-call [cursor]
    {:body (take 2 (drop cursor (range 10)))
     :next-cursor (when-not (<= 10 cursor) (+ cursor 2))})

  (loop [cursor 0
         acc []]

    (println (str ";; " cursor))
    (println (str ";; " acc))

    (let [result (api-call cursor)
          body (:body result)
          cursor* (:next-cursor result)
          acc* (apply conj acc body)]

      (println (str ";;; " cursor*))
      (println (str ";;; " acc*))

      (if (some? cursor*)
        (recur cursor* acc*)
        acc)))

  (not (str/blank? ""))

  (if (not-empty [1])
    (println "not empty")
    (println "empty"))

  (not-empty "")

  (or (< 0 (count [1]) 5) (is-empty-page-token? :start))

  (< 0 2 5)
  (> 0 10)
  (not-empty [1])
  (and (< 0 1) (> 5 1))

  (> 0 5)

  (count [])
  (empty? [1])
  (:nextPageToken *1)
  (count (:items *2))

  (conj [] (seq [1 2]))


  (count *1)
  (> 5 (count [1 2 3 5 6]))

  (or (= 1 (count [1])) (is-empty-page-token? :start))
  (get-next-page-token (:next {:next "abc"}))

  (not "hello")
  (vector)

  (is-empty-page-token? "abc")
  (is-not-empty-page-token? "")
  (clojure.string/blank? (str :start))

  (def params {"key" "test" "next" nil})
  (println params)

  (into {} (filter (fn [[_ v]] (some? v)) params))

  (require 'clojure.repl)
  (clojure.repl/doc apply)

  (conj ["a"] (apply conj [1] [2 3]))
  (apply conj '() [2 3 4 5])

  {:nextPageToken "CgkIChiHndSq-C4Qneqg68WZjspc",
   :items [{:id "7356010244958013577",
            :published "2021-03-05T22:44:00+01:00",
            :updated "2021-03-05T22:48:55+01:00",
            :url "http://blog.mrhaki.com/2021/03/gradle-goodness-enabling-preview.html",
            :title "Gradle Goodness: Enabling Preview Features For Java",
            :content "<p>Java introduced preview features in the language since Java 12.
                    This features can be tried out by developers, but are still subject to change and can even be removed in a next release.
                    By default the preview features are not enabled when we want to compile and run our Java code.
                    We must explicitly specify that we want to use the preview feature to the Java compiler and Java runtime using the command-line argument <code>--enable-preview</code>.
                    In Gradle we can customize our build file to enable preview features.
                    We must customize tasks of type <code>JavaCompile</code> and pass <code>--enable-preview</code> to the compiler arguments.
                    Also tasks of type <code>Test</code> and <code>JavaExec</code> must be customized where we need to add the JVM argument <code>--enable-preview</code>.</p>
                    <p>In the following Gradle build script written in Kotlin we have a Java project written with Java 15 where we reconfigure the tasks to enable preview features:</p>
                    <pre class=\"brush:kotlin;\">
                    plugins {
                        java
                        application
                    }
                    
                    repositories {
                        mavenCentral()
                    }
                    
                    dependencies {
                        testImplementation(\"org.junit.jupiter:junit-jupiter-api:5.7.1\")
                        testRuntimeOnly(\"org.junit.jupiter:junit-jupiter-engine:5.7.1\")
                    }
                    
                    application {
                        mainClass.set(\"mrhaki.Patterns\")
                    }
                    
                    tasks {
                        val ENABLE_PREVIEW = \"--enable-preview\"
                    
                        // In our project we have the tasks compileJava and
                        // compileTestJava that need to have the
                        // --enable-preview compiler arguments.
                        withType&lt;JavaCompile&gt;() {
                            options.compilerArgs.add(ENABLE_PREVIEW)
                    
                            // Optionally we can show which preview feature we use.
                            options.compilerArgs.add(\"-Xlint:preview\")
                    
                            // Explicitly setting compiler option --release
                            // is needed when we wouldn't set the
                            // sourceCompatiblity and targetCompatibility
                            // properties of the Java plugin extension.
                            options.release.set(15)
                        }
                    
                        // Test tasks need to have the JVM argument --enable-preview.
                        withType&lt;Test&gt;() {
                            useJUnitPlatform()
                            jvmArgs.add(ENABLE_PREVIEW)
                        }
                    
                        // JavaExec tasks need to have the JVM argument --enable-preview.
                        withType&lt;JavaExec&gt;() {
                            jvmArgs.add(ENABLE_PREVIEW)
                        }
                    }</pre>
                    <p>Written with Gradle 6.8.3</p>
                    ",
            :labels ["Gradle"
                     "Gradle 6.8.3"
                     "Gradle:Goodness"
                     "GradleGoodness:Java"
                     "GradleGoodness:Kotlin"],
            :etag "\"dGltZXN0YW1wOiAxNjE0OTgwOTM1MzI4Cm9mZnNldDogMzYwMDAwMAo\""}]})
