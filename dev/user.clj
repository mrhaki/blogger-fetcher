(ns user
  (:require [blogger-fetcher.system :as system]
            [blogger-fetcher.core :as core]
            [clojure.tools.namespace.repl :refer (refresh)]))

(def system nil)

(defn init
  "Constructs the current development system."
  []
  (alter-var-root #'system
                  (constantly (system/development-system (core/get-config)))))

(defn start
  "Starts the current development system."
  []
  (alter-var-root #'system system/start))

(defn stop
  "Shuts down and destroys the current development system."
  []
  (alter-var-root #'system
                  (fn [s] (when s (system/stop s)))))

(defn go
  "Initializes the current development system and starts it running."
  []
  (init)
  (start))

(defn reset []
  (stop)
  (refresh :after 'user/go))

(comment
  (reset)

  
  (def crawler (system/map->BloggerCrawler {:api-key "api-key" :host "host" :blogger-id "blogger-id"}))
  (.stop crawler)
  (.start crawler)
  #'user/crawler
  
  (:api-key crawler)
  (:crawler crawler)

  
  ;empty
  )
