(ns blogger-fetcher.wiremock
  (:import com.github.tomakehurst.wiremock.WireMockServer
           com.github.tomakehurst.wiremock.core.WireMockConfiguration
           com.github.tomakehurst.wiremock.common.ConsoleNotifier))

(defn config
  "Creates a new instance of WireMockConfiguration"
  ([] (new WireMockConfiguration))
  ([config-map]
   (let [config (new WireMockConfiguration)]
     (when-let [v (:root-dir config-map)] (.usingFilesUnderDirectory config v))
     (when-let [v (:debug config-map)] (.notifier config (new ConsoleNotifier v)))
     (.dynamicPort config)
     config)))

(defn server
  "Create a new instance of WireMockServer"
  ([] (new WireMockServer (config)))
  ([config] (new WireMockServer config)))

(defn start
  "Starts the WireMockServer"
  [server]
  (.start server))

(defn stop
  "Stops the WireMockServer"
  [server]
  (.stop server))

(defn reset
  "Removes all stub mappings and deletes the request log"
  [server]
  (.resetAll server))
