(ns blogger-fetcher.storage-test
  (:require [clojure.test :refer [deftest is testing run-tests]]
            [blogger-fetcher.storage :refer [upsert-post] :as storage]
            [datalevin.core :as d]))

(defn is-valid-storage
  "Assert implementation of Storage protocol"
  [storage]
  (testing "Can store and retrieve a post"
    (let [id "blog-id"
          post {:blog/id      id
                :blog/content {:content "New"}}]
      (testing "Store new post"
        (is (= id (upsert-post storage id post))))
      
      (testing "Update existing post"
        (let [new-post {:blog/id      id
                        :blog/content {:content "Updated"}}]
          (upsert-post storage id post)
          (testing "Existing id is kept"
            (is (= id (upsert-post storage id new-post)))))))))
  
(deftest in-memory-storage-test
  (let [storage (storage/in-memory-storage)]
    (is-valid-storage storage)))

(deftest file-storage-test
  (let [storage (storage/file-storage "./target/posts")]
    (is-valid-storage storage)))

(deftest datalevin-storage-test
  (let [schema {:id {:db/valueType :db.type/string
                     :db/unique    :db.unique/identity}}
        conn (d/get-conn "./target/db" schema)
        storage (storage/datalevin-storage conn)]
    (is-valid-storage storage)
    (d/close conn)))

(comment
  (run-tests)
  ,)

