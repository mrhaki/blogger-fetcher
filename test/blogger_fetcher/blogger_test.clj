(ns blogger-fetcher.blogger-test
  (:require [clojure.test :refer [deftest is testing run-tests use-fixtures]]
            [blogger-fetcher.wiremock :as wiremock]
            [blogger-fetcher.blogger :as blogger]))

(def wiremock-server (wiremock/server (wiremock/config {:root-dir "test-resources" :debug false})))

(use-fixtures :once (fn [test-suite] (wiremock/start wiremock-server) (test-suite) (wiremock/stop wiremock-server)))

(use-fixtures :each (fn [test-to-run] (wiremock/reset wiremock-server) (test-to-run)))

(deftest get-posts
  (let [wiremock-port (.port wiremock-server)
        host (str "http://localhost:" wiremock-port)]

    (testing "Blogger API response with posts"
      (let [crawler {:api-key "api-key"
                     :blogger-id "blogger-id"
                     :host host}
            blogs (blogger/get-posts crawler)
            [post1 post2] blogs]
        (is (= {:id "7356010244958013577"
                :published "2021-03-05T22:44:00+01:00"
                :updated "2021-03-05T22:48:55+01:00"
                :url "http://blog.mrhaki.com/2021/03/gradle-goodness-blog.html"
                :title "Gradle Goodness: Test"
                :content "<h1>Gradle Test</h1>"
                :labels ["Gradle" "Gradle:Goodness"]
                :etag "\"dGltZXN0YW1wOiAxNjE0OTgwOTM1MzI4Cm9mZnNldDogMzYwMDAwMAo\""} post1))
        (is (= {:id "7356010244958013578",
                :published "2021-03-05T22:44:00+01:00"
                :updated "2021-03-05T22:48:55+01:00"
                :url "http://blog.mrhaki.com/2021/03/groovy-goodness-blog.html"
                :title "Groovy Goodness: Test",
                :content "<h1>Groovy Test</h1>"
                :labels ["Groovy" "Groovy:Goodness"]
                :etag "\"dGltZXN0YW1wOiAxNjE0OTgwOTM1MzI4Cm9mZnNldDogMzYwMDAwMA\""} post2))))

    (testing "Blogger API has no posts"
      (let [crawler {:api-key "api-key"
                     :blogger-id "blogger-no-posts"
                     :host host}
            blogs (blogger/get-posts crawler)]
        (is (= blogs []))))))

(comment
  (run-tests)

  (bean wiremock-server)

  (.start wiremock-server)
  (.stop wiremock-server)
  (.port wiremock-server)
  (.getStubMappings wiremock-server))